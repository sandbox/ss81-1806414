eWAY Rapid
==========

This module integrates the Rapid 3.0 (new services, provided by Australian payment gateway eWay) to Ubercart.
http://www.eway.com.au
http://www.eway.com.au/developers/api/rapid-3-0

Installation
============

1. Install & enable the module.

2. Login on http://www.eway.com.au as merchant and get API Key. This FAQ record describes how to do this:
   https://eway.zendesk.com/entries/22370567-how-to-generate-your-live-rapid-3-0-api-key-and-password

3. Enable "Credit Card" payment method on http://example.com/admin/store/settings/payment.

4. In settings of "Credit Card" payment method (http://example.com/admin/store/settings/payment/method/credit) set
   "eWAY Rapid" as default gateway.

5. Set "Payment Username" and "Payment Password" on "eWAY Rapid" tab.
