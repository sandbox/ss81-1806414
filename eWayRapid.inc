<?php
/**
 * @file
 * Contains eWayRapid API.
 */

class eWayRapid {
  /**
   * @var string The Username for HTTP authorization on SOAP server.
   */
  public $httpUsername;

  /**
   * @var string The Password for HTTP authorization on SOAP server.
   */
  public $httpPassword;

  /**
   * @var bool The flag, which enables "sandbox" mode for this class.
   */
  public $sandbox;

  /**
   * @var string The storage for error messages.
   */
  public $error;


  function __construct($username, $password, $sandbox = FALSE) {
    //set_time_limit(0);

    $this->httpUsername = $username;
    $this->httpPassword = $password;
    $this->sandbox = $sandbox;
  }

  /**
   * Returns the URL to the eWAY RapidAPI SOAP service.
   *
   * @return string
   */
  private function getPaymentServiceURL() {
    if ($this->sandbox) {
      return 'https://api.sandbox.ewaypayments.com/Soap.asmx?WSDL';
    }
    else {
      return 'https://api.ewaypayments.com/Soap.asmx?WSDL';
    }
  }

  /**
   * Calls the $service service of SOAP server using $request as request and returns the $response_field field of
   * web service's response.
   *
   * @param string $service
   * @param StdClass $request
   * @param string$response_field
   * @return StdClass|bool
   */
  private function soapCall($service, $request, $response_field) {
    $this->error = '';

    try {
      $client = new SoapClient($this->getPaymentServiceURL(), array(
        'trace' => TRUE,
        'exceptions' => TRUE,
        'login' => $this->httpUsername,
        'password' => $this->httpPassword
      ));

      $result = $client->{$service}(array('request' => get_object_vars($request)));
    }
    catch (Exception $e) {
      $this->error = t('@service SOAP Error: @error', array('@service' => $service, '@error' => $e->getMessage()));
    }

    $logger = new CarefulLogger();

    if (empty($this->error)) {
      $logger->logXML("{$service} Request", $client->__getLastRequest());
      $logger->logXML("{$service} Response", $client->__getLastResponse());

      return $result->{$response_field};
    }
    else {
      return FALSE;
    }
  }

  /**
   * Calls "Create Access Code" web service.
   *
   * @param CreateAccessCodeRequest $request
   * @return StdClass or bool
   */
  public function CreateAccessCode($request) {
    return $this->soapCall('CreateAccessCode', $request, 'CreateAccessCodeResult');
  }

  /**
   * Calls "Get Access Code Result" web service.
   *
   * @param GetAccessCodeResultRequest $request
   * @return StdClass|bool
   */
  private function GetAccessCodeResult($request) {
    return $this->soapCall('GetAccessCodeResult', $request, 'GetAccessCodeResultResult');
  }

  /**
   * @param $fieldsValues
   * @return CheckoutResponse|bool
   */
  public function submitToFormActionURL($fieldsValues) {
    require_once 'HTTP/Request2.php';
    require_once 'HTTP/Request2/Response.php';
    require_once 'HTTP/Request2/CookieJar.php';

    // Prepare fields of the form.
    $formFields = array(
      'EWAY_ACCESSCODE' => $fieldsValues->AccessCode,
      'EWAY_CARDNAME' => $fieldsValues->CardOwner,
      'EWAY_CARDNUMBER' => $fieldsValues->CardNumber,
      'EWAY_CARDEXPIRYMONTH' => $fieldsValues->CardExpMonth,
      'EWAY_CARDEXPIRYYEAR' => $fieldsValues->CardExpYear,
      'EWAY_CARDSTARTMONTH' => '',
      'EWAY_CARDSTARTYEAR' => '',
      'EWAY_CARDISSUENUMBER' => !empty($fieldsValues->CardIssueNumber) ? $fieldsValues->CardIssueNumber : '22',
      'EWAY_CARDCVN' => $fieldsValues->CardCVV,
    );

    $logger = new CarefulLogger();
    $logger->logArray('FormActionURL Request', $formFields);

    // Submit the form.
    $httpOut = new HTTP_Request2($fieldsValues->FormActionURL);
    $httpOut->setMethod(HTTP_Request2::METHOD_POST)->addPostParameter($formFields);
    $httpOut->setConfig(array('ssl_verify_peer' => FALSE, 'ssl_verify_host' => FALSE, 'follow_redirects' => TRUE));

    $httpIn = $httpOut->send();

    $logger->logArray('FormActionURL Response', $httpIn);

    // Check result.
    if ($httpIn->getStatus() != '200') {
      $this->error = t('FormActionURL Form Error: @error', array('@error' => print_r($httpIn, 1)));
      return FALSE;
    }

    $url = $httpIn->geteffectiveUrl();

    if (empty($url)) {
      $this->error = t('FormActionURL Form Error: the "effectiveUrl" is missed. HTTP Response: @http', array('@http' => print_r($httpIn, 1)));
      return FALSE;
    }

    parse_str(parse_url($url, PHP_URL_QUERY), $query);

    if (empty($query['AccessCode'])) {
      $this->error = t('FormActionURL Form Error: the "AccessCode" is missed. HTTP Response: @http', array('@http' => print_r($httpIn, 1)));
      return FALSE;
    }

    // Build request for getting the result with the access code.
    $request = new GetAccessCodeResultRequest();
    $request->AccessCode = $query['AccessCode'];

    // Get the result.
    $result = $this->GetAccessCodeResult($request);

    // Create CheckoutResponse Object.
    $final = new CheckoutResponse();
    $final->CheckoutResponse = $result;

    $final->parseResponseMessage();

    return $final;
  }
}

class CreateAccessCodeRequest {
  public $RedirectUrl;
  public $CustomerIP;
  public $Method;
  //public $DeviceID;
  public $Payment;
  public $Customer;
  public $ShippingAddress;
  public $Items;
  public $Options;

  function __construct() {
    $this->Payment = new Payment();
    $this->Customer = new Customer();
    $this->ShippingAddress = new ShippingAddress();

    $this->CustomerIP = $_SERVER['REMOTE_ADDR'];

    /**
     * URL to the page for getting the result with an AccessCode.
     * We use FRONT page, because we parse HTTP response from eWAY, so we don't need to do anything on RedirectUrl page.
     */
    $this->RedirectUrl = url(drupal_get_path('module', 'uc_eway_rapid') . '/empty-redirect-page.php', array('absolute' => TRUE));
    //$this->RedirectUrl = url('<front>', array('absolute' => TRUE));
  }
}

class Payment {
  //<Payment>
  //  <TotalAmount>1000</TotalAmount>
  //  <InvoiceNumber>19832261</InvoiceNumber>
  //  <InvoiceDescription>Online Purchase</InvoiceDescription>
  //  <InvoiceReference>19832261-AA12/1</InvoiceReference>
  //  <CurrencyCode>AUD</CurrencyCode>
  //</Payment>

  public $TotalAmount;
  public $InvoiceNumber;
  public $InvoiceDescription;
  public $InvoiceReference;
  public $CurrencyCode;
}

class Customer {
  //<Customer>
  //  <Reference>Test123</Reference>
  //  <Title>Mr.</Title>
  //  <FirstName>John</FirstName>
  //  <LastName>Smith</LastName>
  //  <CompanyName>eWAY</CompanyName>
  //  <JobDescription></JobDescription>
  //  <Street1>Unit 4</Street1>
  //  <Street2>15 Smith St</Street2>
  //  <City>Sydney</City>
  //  <State>NSW</State>
  //  <PostalCode>2000</PostalCode>
  //  <Country>au</Country>
  //  <Email>sales@dummyshop123.com</Email>
  //  <Phone>1800106565</Phone>
  //  <Mobile>1800106565</Mobile>
  //  <Comments>Customer comments</Comments>
  //  <Fax>1800106565</Fax>
  //  <Url>http://www.dummyshop123.com</Url>
  //</Customer>

  public $TokenCustomerID;
  public $Reference;
  public $Title;
  public $FirstName;
  public $LastName;
  public $CompanyName;
  public $JobDescription;
  public $Street1;
  public $Street2;
  public $City;
  public $State;
  public $PostalCode;
  public $Country;
  public $Email;
  public $Phone;
  public $Mobile;
  public $Comments;
  public $Fax;
  public $Url;
}

class ShippingAddress {
  //<ShippingAddress>
  //  <ShippingMethod>NextDay</ShippingMethod>
  //  <FirstName>John</FirstName>
  //  <LastName>Smith</LastName>
  //  <Street1>Unit 4</Street1>
  //  <Street2>15 Smith St</Street2>
  //  <City>Sydney</City>
  //  <State>NSW</State>
  //  <PostalCode>2000</PostalCode>
  //  <Country>au</Country>
  //  <Email>sales@dummyshop123.com</Email>
  //  <Phone>1800106565</Phone>
  //  <Fax>1800106565</Fax>
  //</ShippingAddress>

  public $ShippingMethod;
  public $FirstName;
  public $LastName;
  public $Street1;
  public $Street2;
  public $City;
  public $State;
  public $PostalCode;
  public $Country;
  public $Email;
  public $Phone;
  public $Fax;
}

class Items {
  public $LineItem = array();
}

class LineItem {
  //<LineItem>
  //  <SKU>123456-99</SKU>
  //  <Description>Red Socks</Description>
  //  <Quantity>1</Quantity>
  //  <UnitCost>909</UnitCost>
  //  <Tax>91</Tax>
  //  <Total>1000</Total>
  //</LineItem>

  public $SKU;
  public $Description;
  public $Quantity;
  public $UnitCost;
  public $Tax;
  public $Total;
}

class Options {
  public $Option = array();
}

class Option {
  public $Value;
}

class GetAccessCodeResultRequest {
  public $AccessCode;
}

class FormActionURL {
  public $FormActionURL;
  public $AccessCode;
  public $CardOwner;
  public $CardNumber;
  public $CardExpMonth;
  public $CardExpYear;
  public $CardIssueNumber;
  public $CardCVV;
}

class CheckoutResponse {
  public $CheckoutResponse;

  // Messages, provided by each validation type.
  public $ValidationMessages;
  public $BeagleAlertsMessages;
  public $TransactionMessage;

  // Status of each validation type.
  public $TransactionStatus;
  public $BeagleStatus;
  public $ValidationStatus;

  // Final status of checkout.
  public $CheckoutStatus;

  private $ValidationResponseCodes = array(
    'V6000' => 'Validation error',
    'V6001' => 'Invalid CustomerIP',
    'V6002' => 'Invalid DeviceID',
    'V6011' => 'Invalid Payment TotalAmount',
    'V6012' => 'Invalid Payment InvoiceDescription',
    'V6013' => 'Invalid Payment InvoiceNumber',
    'V6014' => 'Invalid Payment InvoiceReference',
    'V6015' => 'Invalid Payment CurrencyCode',
    'V6016' => 'Payment Required',
    'V6017' => 'Payment CurrencyCode Required',
    'V6018' => 'Unknown Payment CurrencyCode',
    'V6021' => 'EWAY_CARDHOLDERNAME Required',
    'V6022' => 'EWAY_CARDNUMBER Required',
    'V6023' => 'EWAY_CARDCVN Required',
    'V6033' => 'Invalid Expiry Date',
    'V6034' => 'Invalid Issue Number',
    'V6035' => 'Invalid Valid From Date',
    'V6040' => 'Invalid TokenCustomerID',
    'V6041' => 'Customer Required',
    'V6042' => 'Customer FirstName Required',
    'V6043' => 'Customer LastName Required',
    'V6044' => 'Customer CountryCode Required',
    'V6045' => 'Customer Title Required',
    'V6046' => 'TokenCustomerID Required',
    'V6047' => 'RedirectURL Required',
    'V6051' => 'Invalid Customer FirstName',
    'V6052' => 'Invalid Customer LastName',
    'V6053' => 'Invalid Customer CountryCode',
    'V6054' => 'Invalid Customer Email',
    'V6058' => 'Invalid Customer Title',
    'V6059' => 'Invalid RedirectURL',
    'V6060' => 'Invalid TokenCustomerID',
    'V6061' => 'Invalid Customer Reference',
    'V6062' => 'Invalid Customer CompanyName',
    'V6063' => 'Invalid Customer JobDescription',
    'V6064' => 'Invalid Customer Street1',
    'V6065' => 'Invalid Customer Street2',
    'V6066' => 'Invalid Customer City',
    'V6067' => 'Invalid Customer State',
    'V6068' => 'Invalid Customer PostalCode',
    'V6069' => 'Invalid Customer Email',
    'V6070' => 'Invalid Customer Phone',
    'V6071' => 'Invalid Customer Mobile',
    'V6072' => 'Invalid Customer Comments',
    'V6073' => 'Invalid Customer Fax',
    'V6074' => 'Invalid Customer URL',
    'V6075' => 'Invalid ShippingAddress FirstName',
    'V6076' => 'Invalid ShippingAddress LastName',
    'V6077' => 'Invalid ShippingAddress Street1',
    'V6078' => 'Invalid ShippingAddress Street2',
    'V6079' => 'Invalid ShippingAddress City',
    'V6080' => 'Invalid ShippingAddress State',
    'V6081' => 'Invalid ShippingAddress PostalCode',
    'V6082' => 'Invalid ShippingAddress Email',
    'V6083' => 'Invalid ShippingAddress Phone',
    'V6084' => 'Invalid ShippingAddress Country',
    'V6091' => 'Unknown Customer CountryCode',
    'V6100' => 'Invalid EWAY_CARDNAME',
    'V6101' => 'Invalid EWAY_CARDEXPIRYMONTH',
    'V6102' => 'Invalid EWAY_CARDEXPIRYYEAR',
    'V6103' => 'Invalid EWAY_CARDSTARTMONTH',
    'V6104' => 'Invalid EWAY_CARDSTARTYEAR',
    'V6105' => 'Invalid EWAY_CARDISSUENUMBER',
    'V6106' => 'Invalid EWAY_CARDCVN',
    'V6107' => 'Invalid EWAY_ACCESSCODE',
    'V6108' => 'Invalid CustomerHostAddress',
    'V6109' => 'Invalid UserAgent',
    'V6110' => 'Invalid EWAY_CARDNUMBER'
  );

  private $BeagleAlertsFraudResponseMessages = array(
    'F9010' => 'High Risk Billing Country',
    'F9011' => 'High Risk Credit Card Country',
    'F9012' => 'High Risk Customer IP Address',
    'F9013' => 'High Risk Email Address',
    'F9014' => 'High Risk Shipping Country',
    'F9015' => 'Multiple card numbers for single email address',
    'F9016' => 'Multiple card numbers for single location',
    'F9017' => 'Multiple email addresses for single card number',
    'F9018' => 'Multiple email addresses for single location',
    'F9019' => 'Multiple locations for single card number',
    'F9020' => 'Multiple locations for single email address',
    'F9021' => 'Suspicious Customer First Name',
    'F9022' => 'Suspicious Customer Last Name',
    'F9023' => 'Transaction Declined',
    'F9024' => 'Multiple transactions for same address with known credit card',
    'F9025' => 'Multiple transactions for same address with new credit card',
    'F9026' => 'Multiple transactions for same email with new credit card',
    'F9027' => 'Multiple transactions for same email with known credit card',
    'F9028' => 'Multiple transactions for new credit card',
    'F9029' => 'Multiple transactions for known credit card',
    'F9030' => 'Multiple transactions for same email address',
    'F9031' => 'Multiple transactions for same credit card',
    'F9032' => 'Invalid Customer Last Name',
    'F9033' => 'Invalid Billing Street',
    'F9034' => 'Invalid Shipping Street',
  );

  /**
   * @var array The list of all the possible response messages returned from the Rapid 3.0 and the relating result.
   */
  private $TransactionResponseMessages = array(
    'A2000' => array('Transaction Approved', 'Successful'),
    'A2008' => array('Honor With Identification', 'Successful'),
    'A2010' => array('Approved For Partial Amount', 'Successful'),
    'A2011' => array('Approved, VIP', 'Successful'),
    'A2016' => array('Approved, Update Track 3', 'Successful'),
    'D4401' => array('Refer to Issuer', 'Failed'),
    'D4402' => array('Refer to Issuer, Special', 'Failed'),
    'D4403' => array('No Merchant', 'Failed'),
    'D4404' => array('Pick Up Card', 'Failed'),
    'D4405' => array('Do Not Honor', 'Failed'),
    'D4406' => array('Error', 'Failed'),
    'D4407' => array('Pick Up Card, Special', 'Failed'),
    'D4409' => array('Request In Progress', 'Failed'),
    'D4412' => array('Invalid Transaction', 'Failed'),
    'D4413' => array('Invalid Amount', 'Failed'),
    'D4414' => array('Invalid Card Number', 'Failed'),
    'D4415' => array('No Issuer', 'Failed'),
    'D4419' => array('Re-enter Last Transaction', 'Failed'),
    'D4421' => array('No Action Taken', 'Failed'),
    'D4422' => array('Suspected Malfunction', 'Failed'),
    'D4423' => array('Unacceptable Transaction Fee', 'Failed'),
    'D4425' => array('Unable to Locate Record On File', 'Failed'),
    'D4430' => array('Format Error', 'Failed'),
    'D4431' => array('Bank Not Supported By Switch', 'Failed'),
    'D4433' => array('Expired Card, Capture', 'Failed'),
    'D4434' => array('Suspected Fraud, Retain Card', 'Failed'),
    'D4435' => array('Card Acceptor, Contact Acquirer, Retain Card', 'Failed'),
    'D4436' => array('Restricted Card, Retain Card', 'Failed'),
    'D4437' => array('Contact Acquirer Security Department, Retain Card', 'Failed'),
    'D4438' => array('PIN Tries Exceeded, Capture', 'Failed'),
    'D4439' => array('No Credit Account', 'Failed'),
    'D4440' => array('Function Not Supported', 'Failed'),
    'D4441' => array('Lost Card', 'Failed'),
    'D4442' => array('No Universal Account', 'Failed'),
    'D4443' => array('Stolen Card', 'Failed'),
    'D4444' => array('No Investment Account', 'Failed'),
    'D4451' => array('Insufficient Funds', 'Failed'),
    'D4452' => array('No Cheque Account', 'Failed'),
    'D4453' => array('No Savings Account', 'Failed'),
    'D4454' => array('Expired Card', 'Failed'),
    'D4455' => array('Incorrect PIN', 'Failed'),
    'D4456' => array('No Card Record', 'Failed'),
    'D4457' => array('Function Not Permitted to Cardholder', 'Failed'),
    'D4458' => array('Function Not Permitted to Terminal', 'Failed'),
    'D4459' => array('Suspected Fraud', 'Failed'),
    'D4460' => array('Acceptor Contact Acquirer', 'Failed'),
    'D4461' => array('Exceeds Withdrawal Limit', 'Failed'),
    'D4462' => array('Restricted Card', 'Failed'),
    'D4463' => array('Security Violation', 'Failed'),
    'D4464' => array('Original Amount Incorrect', 'Failed'),
    'D4466' => array('Acceptor Contact Acquirer, Security', 'Failed'),
    'D4467' => array('Capture Card', 'Failed'),
    'D4475' => array('PIN Tries Exceeded', 'Failed'),
    'D4482' => array('CVV Validation Error', 'Failed'),
    'D4490' => array('Cutoff In Progress', 'Failed'),
    'D4491' => array('Card Issuer Unavailable', 'Failed'),
    'D4492' => array('Unable To Route Transaction', 'Failed'),
    'D4493' => array('Cannot Complete, Violation Of The Law', 'Failed'),
    'D4494' => array('Duplicate Transaction', 'Failed'),
    'D4496' => array('System Error', 'Failed'),
  );

  public function parseResponseMessage() {
    $this->ValidationMessages = $this->BeagleAlertsMessages = $this->TransactionMessage = array();

    $messages = explode(', ', $this->CheckoutResponse->ResponseMessage);

    for ($i = 0; $i < count($messages); $i++) {
      switch ($messages[$i][0]) {
        case 'V':
          $source = 'ValidationResponseCodes';
          $destination = 'ValidationMessages';
          break;

        case 'F';
          $source = 'BeagleAlertsFraudResponseMessages';
          $destination = 'BeagleAlertsMessages';
          break;

        case 'A';
        case 'D';
          $source = 'TransactionResponseMessages';
          $destination = 'TransactionMessage';
          break;

        default:
          return t('Unknown message: @msg', array('@msg' => $messages[$i]));
      }

      if (!empty($this->{$source}[$messages[$i]])) {
        $this->{$destination}[$messages[$i]] = $this->{$source}[$messages[$i]];
      }
      else {
        $this->{$destination}[$messages[$i]] = $messages[$i];
      }
    }

    foreach ($this->TransactionMessage as $k => $v) {
      $this->TransactionStatus = $v[1] == 'Successful' ? 1 : 0;
      $this->TransactionMessage[$k] = $v[0];
    }

    $this->BeagleStatus = empty($this->BeagleAlertsMessages) ? 1 : 0;

    $this->CheckoutStatus = $this->TransactionStatus && $this->BeagleStatus ? 1 : 0;

    return TRUE;
  }

  public function getTransactionMessage() {
    static $message;

    if (empty($message)) {
      $message = array_values($this->TransactionMessage);

      if (!empty($message[0])) {
        $message = $message[0];
      }
    }

    return $message;
  }
}

class CarefulLogger {
  private $bc;
  private $enabled = FALSE;

  function __construct() {
    // TODO Use load_library();
    require_once 'sites/all/libraries/beautyxml/BeautyXML.class.php';
    $this->bc = new BeautyXML();
  }

  public function logXML($title, $xml) {
    if ($this->enabled) {
      $fp = fopen('sites/default/files/CarefulLogger.log', 'a');
      fputs($fp, $title . "\n" . $this->bc->format($xml) . "\n\n\n");
      fclose($fp);
    }
  }

  public function logArray($title, $data) {
    if ($this->enabled) {
      $fp = fopen('sites/default/files/CarefulLogger.log', 'a');
      fputs($fp, $title . "\n" . print_r($data, 1) . "\n\n\n");
      fclose($fp);
    }
  }
}
